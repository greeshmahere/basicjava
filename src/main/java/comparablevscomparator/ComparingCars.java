package comparablevscomparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ComparingCars {
    public static void main(String[] args)
    {
        List<Car> cars = new ArrayList<Car>();
        cars.add(new Car("Toyata",15000,7000));
        cars.add(new Car("Kia",30000,15000));
        cars.add(new Car("Hyundai",10000,8000));

        Collections.sort(cars);

        //Comparable and comparator can be used for comparing

        for( Car car:cars)
        {
            System.out.println(car);
        }
    }

}
