package comparablevscomparator;

public class Car implements Comparable<Car>
{
private String carModel;
private double mileage;
private double price;

    public Car(String carModel, double mileage, double price) {
        this.carModel = carModel;
        this.mileage = mileage;
        this.price = price;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public double getMileage() {
        return mileage;
    }

    public void setMileage(double mileage) {
        this.mileage = mileage;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Car{" +
                "carModel='" + carModel + '\'' +
                ", mileage=" + mileage +
                ", price=" + price +
                '}';
    }

    public int compareTo(Car car2) {
        if (this.getMileage()>car2.getMileage())
             return 1;
        else
            return -1;
    }
}
